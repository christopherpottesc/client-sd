import { Line } from "vue-chartjs";

export default {
  extends: Line,
  props: {
    chartdata: {
      type: Object,
      default: null
    },
    legend: {
      type: Boolean,
      default: true
    }
  },
  data: () => ({
    options: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      },
      legend: {
        display: true
      }
    }
  }),
  mounted() {
    this.options.legend.display = this.legend;
    this.renderChart(this.chartdata, this.options);
  }
};
